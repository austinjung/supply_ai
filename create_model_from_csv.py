import sys
import os

sys.path.extend(['/api_app'])
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "api_engineering.settings")

import django
django.setup()


import csv
from api_app.models import ShippedPackage
from datetime import datetime
import pytz
from decimal import Decimal


if __name__ == '__main__':
    with open('data.csv') as f:
        reader = csv.reader(f)
        for row in reader:
            try:
                sp = ShippedPackage()
                sp.index = int(row[0])
                sp.awb = int(row[1])
                sp.breadth = int(row[2])
                sp.buyer_city = row[3]
                sp.buyer_lin = row[4]
                if row[5] not in ['', None]:
                    sp.cancelled_date = pytz.UTC.localize(datetime.strptime(row[5], '%Y-%m-%d %H:%M:%S'))
                sp.current_status = row[6]
                if row[7] not in ['', None]:
                    sp.delivered_date = pytz.UTC.localize(datetime.strptime(row[7], '%Y-%m-%d %H:%M:%S'))
                sp.delivery_attempt_count = int(row[8])
                if row[9] not in ['', None]:
                    sp.dispatch_date = pytz.UTC.localize(datetime.strptime(row[9], '%Y-%m-%d %H:%M:%S'))
                if row[10] in ['TRUE', 'True']:
                    sp.heavy = True
                sp.height = int(row[11])
                if row[12] not in ['', None]:
                    sp.last_mile_arrival_date = pytz.UTC.localize(datetime.strptime(row[12], '%Y-%m-%d %H:%M:%S'))
                if row[13] not in ['', None]:
                    sp.last_modified = pytz.UTC.localize(datetime.strptime(row[13], '%Y-%m-%d %H:%M:%S'))
                sp.length = int(row[14])
                if row[15] not in ['', None]:
                    sp.order_created_date = pytz.UTC.localize(datetime.strptime(row[15], '%Y-%m-%d %H:%M:%S'))
                sp.order_id = row[16]
                sp.price = Decimal(row[17])
                sp.product_category = row[18]
                sp.product_id = row[19]
                sp.product_name = row[20]
                sp.product_price = Decimal(row[21])
                sp.product_qty = int(row[22])
                if row[23] not in ['', None]:
                    sp.promised_date = pytz.UTC.localize(datetime.strptime(row[23], '%Y-%m-%d %H:%M:%S'))
                sp.return_cause = row[24]
                if row[25] not in ['', None]:
                    sp.reverse_logistics_booked_date = pytz.UTC.localize(datetime.strptime(row[25], '%Y-%m-%d %H:%M:%S'))
                if row[26] not in ['', None]:
                    sp.reverse_logistics_date = pytz.UTC.localize(datetime.strptime(row[26], '%Y-%m-%d %H:%M:%S'))
                if row[27] not in ['', None]:
                    sp.reverse_logistics_delivered_date = pytz.UTC.localize(datetime.strptime(row[27], '%Y-%m-%d %H:%M:%S'))
                if row[28] not in ['', None]:
                    sp.rto_date = pytz.UTC.localize(datetime.strptime(row[28], '%Y-%m-%d %H:%M:%S'))
                if row[29] not in ['', None]:
                    sp.rto_delivered_date = pytz.UTC.localize(datetime.strptime(row[29], '%Y-%m-%d %H:%M:%S'))
                sp.seller_city = row[30]
                sp.seller_pin = row[31]
                if row[32] not in ['', None]:
                    sp.shipper_confirmation_date = pytz.UTC.localize(datetime.strptime(row[32], '%Y-%m-%d %H:%M:%S'))
                sp.shipper_name = row[33]
                sp.shipping_cost = Decimal(row[34])
                sp.weight = int(row[35])
                sp.save()
            except Exception as e:
                pass
        pass
