"""api_engineering URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import include, url
from django.contrib import admin
from rest_framework import routers as drf_routers
from api_app.views import ShippedPackageViewSet, APIViewSet
from django.views.generic import RedirectView

api_root_router = drf_routers.SimpleRouter(trailing_slash=False)
api_root_router.register(r'', APIViewSet, base_name='api_endpoint')
api_router = drf_routers.SimpleRouter(trailing_slash=True)
api_router.register(r'shipped_packages', ShippedPackageViewSet, base_name='shipped_packages')

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^$', RedirectView.as_view(url='/api/')),
    url(r'^api/', include(api_root_router.urls)),
    url(r'^api/', include(api_router.urls)),
]
