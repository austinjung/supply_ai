# API Engineer Role

You are required to build and test two sets of API. One Ingest, the other Output.

### Ingest

Design and build an API to ingest the accompanying `data.csv` file in 2 ways

1. Row by row
2. Multiple rows at a time

The primary key is the field `order_id`

The ingested rows should be written to a relational database (preferably postgres)
**DO NOT** read in data using any read csv commands. The API must be used to ingest the data in the csv into the database.  

_Test_: You should be able to `POST` a record or group of records to the API endpoint.

### Output
Design and build an API to output

1. the `shipper_name` given the following parameters:
    1. `order_id`
    2. `seller_location`, `buyer_location`
    3. `seller_location`, `buyer_location`, `product_category`

2. the count of orders made on each `order_created_date` (yyyy-mm-dd) given the following parameters:
    1. `shipper_name`

The APIs will be returning data from the same table (not csv) that you have previously ingested.
You may build a smaller, relevant table that might be convenient for the data being returned.

The API #1 should be able to output shipper names for 1, 2 and 3:

1. Row by row
2. In bulk, where the parameters are passed as a list of lists

_Test_: You should be able to receive the correct data on a `GET` request.

Design the API endpoints to return the relevant error messages.  
The data dictionary is provided to help you with some context. We don't think this has any impact on the API performance :-)

Your submission will be code and documentation submitted as a private gitlab or github repo.
We will test the code by hosting the app and sending `GET` and `POST` requests to it.