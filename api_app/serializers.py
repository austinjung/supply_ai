from rest_framework import serializers
from api_app.models import ShippedPackage
from rest_framework.validators import UniqueValidator
from django.core.validators import MinValueValidator


class ShippedPackageSerializer(serializers.ModelSerializer):
    index = serializers.IntegerField(validators=[UniqueValidator(queryset=ShippedPackage.objects.all())])
    awb = serializers.IntegerField(validators=[UniqueValidator(queryset=ShippedPackage.objects.all())])
    breadth = serializers.IntegerField(validators=[MinValueValidator(1)])
    buyer_city = serializers.CharField(max_length=256)
    buyer_pin = serializers.CharField(max_length=10)
    delivery_attempt_count = serializers.IntegerField(validators=[MinValueValidator(0)])
    height = serializers.IntegerField(validators=[MinValueValidator(1)])
    length = serializers.IntegerField(validators=[MinValueValidator(1)])
    order_id = serializers.CharField(validators=[UniqueValidator(queryset=ShippedPackage.objects.all())])
    product_id = serializers.CharField(validators=[UniqueValidator(queryset=ShippedPackage.objects.all())])
    product_name = serializers.CharField(max_length=256)
    product_qty = serializers.IntegerField(validators=[MinValueValidator(0)])
    seller_city = serializers.CharField(max_length=256)
    seller_pin = serializers.CharField(max_length=10)
    shipper_name = serializers.CharField(max_length=256)
    weight = serializers.IntegerField(validators=[MinValueValidator(1)])

    class Meta:
        model =ShippedPackage
        fields = '__all__'


class ShipperNameSerializer(serializers.ModelSerializer):
    shipper_name = serializers.CharField(max_length=256)

    class Meta:
        model =ShippedPackage
        fields = ('shipper_name', )
