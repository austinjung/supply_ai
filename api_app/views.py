from django_filters.filters import CharFilter
from rest_framework import viewsets
from rest_framework.response import Response
from rest_framework import status
from rest_framework import filters
from rest_framework.decorators import list_route
from api_app.utils import ShowUrls
from api_app.serializers import ShippedPackageSerializer, ShipperNameSerializer
from api_app.models import ShippedPackage
from datetime import date


class APIViewSet(viewsets.ViewSet):
    """
    API endpoint urls
    """
    def list(self, request, *args, **kwargs):
        return Response(ShowUrls().get_endpoints(request))


class ShippedPackageFilter(filters.FilterSet):
    order_id = CharFilter(name="order_id", lookup_expr='exact')
    seller_location = CharFilter(name="seller_city", lookup_expr='exact')
    buyer_location = CharFilter(name="buyer_city", lookup_expr='exact')
    product_category = CharFilter(name="product_category", lookup_expr='exact')
    shipper_name = CharFilter(name="shipper_name", lookup_expr='exact')

    class Meta:
        model = ShippedPackage
        fields = ('order_id', 'seller_location', 'buyer_location', 'product_category', 'shipper_name')


class ShippedPackageViewSet(viewsets.ModelViewSet):
    queryset = ShippedPackage.objects.all()
    serializer_class = ShippedPackageSerializer
    filter_class = ShippedPackageFilter
    ordering_fields = ('order_id', 'order_created_date')
    ordering = ('order_id', )

    def get_serializer_class(self):
        if self.action == 'shipper_names':
            return ShipperNameSerializer
        else:
            return super(ShippedPackageViewSet, self).get_serializer_class()

    """
    Create a model instance.
    """
    def create(self, request, *args, **kwargs):
        if isinstance(request.data, list):
            bulk_created_response = []
            for i in range(len(request.data)):
                serializer = self.get_serializer(data=request.data[i])
                serializer.is_valid(raise_exception=True)
                self.perform_create(serializer)
                bulk_created_response.append(serializer.data)
                if i == len(request.data) - 1:
                    headers = self.get_success_headers(serializer.data)
                    return Response(bulk_created_response, status=status.HTTP_201_CREATED, headers=headers)
        else:
            serializer = self.get_serializer(data=request.data)
            serializer.is_valid(raise_exception=True)
            self.perform_create(serializer)
            headers = self.get_success_headers(serializer.data)
            return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)

    """
    Get shipper names filter by 'order_id', 'seller_location', 'buyer_location', 'product_category'
    """
    @list_route(methods=['head', 'options', 'get'])
    def shipper_names(self, request, *args, **kwargs):
        queryset = self.filter_queryset(self.get_queryset()).order_by('shipper_name').distinct('shipper_name')

        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)

    """
    Get number of orders by date
    """
    @list_route(methods=['head', 'options', 'get'])
    def number_of_orders(self, request, *args, **kwargs):
        queryset = self.filter_queryset(self.get_queryset())

        number_of_orders_by_date = {}
        for pkg in queryset:
            order_created_date = date(year=pkg.order_created_date.year, month=pkg.order_created_date.month, day=pkg.order_created_date.day).isoformat()
            if order_created_date in number_of_orders_by_date:
                number_of_orders_by_date[order_created_date] += 1
            else:
                number_of_orders_by_date[order_created_date] = 1

        return Response(number_of_orders_by_date)

