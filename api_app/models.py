from __future__ import unicode_literals

from django.db import models
from django.core.validators import MinValueValidator

# Create your models here.
class ShippedPackage(models.Model):
    STATUS_CHOICES = (
        ('CAN', 'Canceled'),
        ('CTD', 'CTD'),
        ('DEL', 'Delivered'),
        ('ITR', 'ITR'),
        ('RTN', 'Returned'),
        ('RTO', 'Returned to origin'),
        ('SHP', 'Shipped')
    )
    PRODUCT_CATEGORY_STATUS_CHOICE = (
        ('Clothing', 'Clothing'),
        ('ComputersandPeripherals', 'ComputersandPeripherals'),
        ('Decoratives', 'Decoratives'),
        ('Fitness', 'Fitness'),
        ('Furnishing', 'Furnishing'),
        ('Luggage', 'Luggage'),
        ('Mobiles', 'Mobiles'),
        ('PowerBanks', 'PowerBanks'),
        ('Sports', 'Sports'),
        ('Tablets', 'Tablets')
    )
    RETURN_CAUSE_CHOICE = (
        ('COD_denied', 'COD denied'),
        ('damaged_product', 'damaged product'),
        ('does_not_fit', 'does not fit'),
        ('incorrect_address', 'incorrect address'),
        ('order_cancelled', 'order cancelled'),
        ('others', 'others'),
        ('person_unavailable', 'person unavailable')
    )
    index = models.IntegerField(unique=True)  # just a serial index.
    awb = models.IntegerField(unique=True)  # airway bill - a tracking id for a shipped package : should be unique
    breadth = models.IntegerField(MinValueValidator(1))  # breadth of the package : should be greater than equal 1
    buyer_city = models.CharField(max_length=256)  # city name of the buyer
    buyer_pin = models.CharField(max_length=10)  # zip code of the buyer
    cancelled_date = models.DateTimeField(blank=True, null=True)  # date on which package was cancelled : allow NULL
    current_status = models.CharField(max_length=3, choices=STATUS_CHOICES)  # current status of the package
    delivered_date = models.DateTimeField(blank=True, null=True)  # date on which package was delivered
    delivery_attempt_count = models.IntegerField(MinValueValidator(0))  # number of times delivery was attempted
    dispatch_date = models.DateTimeField(blank=True, null=True)  # date on which package was dispatched.
    heavy = models.BooleanField(default=False)  # flag to check if package is heavy or bulky.
    height = models.IntegerField(MinValueValidator(1))  # height of the package
    last_mile_arrival_date = models.DateTimeField(blank=True, null=True)  # date on which package arrived at the last mile delivery location
    last_modified = models.DateTimeField()  # timestamp when the record was last modified
    length = models.IntegerField(MinValueValidator(1))  # length of the package
    order_created_date = models.DateTimeField()  # date on which package was ordered
    order_id = models.CharField(unique=True, primary_key=True, max_length=256)  # unique identifier for an order
    price = models.DecimalField(decimal_places=2, max_digits=12)  # price of the order
    product_category = models.CharField(max_length=128, choices=PRODUCT_CATEGORY_STATUS_CHOICE)  # category of the order
    product_id = models.CharField(unique=True, max_length=256)  # unique identifier for the product in the package
    product_name = models.CharField(max_length=256)  # name of the product in the package
    product_price = models.DecimalField(decimal_places=2, max_digits=12)  # price of product in the package
    product_qty = models.IntegerField(MinValueValidator(0))  # quantity of product(s) in the package
    promised_date = models.DateTimeField(blank=True, null=True)  # date on which package was promised to be delivered
    return_cause = models.CharField(max_length=128, choices=RETURN_CAUSE_CHOICE)  # reason for returning the product/package
    reverse_logistics_booked_date = models.DateTimeField(blank=True, null=True)  # date on which package was booked for return
    reverse_logistics_date = models.DateTimeField(blank=True, null=True)  # date on which package was picked up for returning
    reverse_logistics_delivered_date = models.DateTimeField(blank=True, null=True)  # date on which package was returned to sender
    rto_date = models.DateTimeField(blank=True, null=True)  # date on which package was set for RTO. RTO stands for return to origin. This happens when the package is undelivered.
    rto_delivered_date = models.DateTimeField(blank=True, null=True)  # date on which RTO was completed by returning to sender
    seller_city = models.CharField(max_length=256)  # city name of the seller
    seller_pin = models.CharField(max_length=10)  # zip code of the seller
    shipper_confirmation_date = models.DateTimeField(blank=True, null=True)  # date on which carrier confirmed pickup
    shipper_name = models.CharField(max_length=256)  # name of carrier (e.g.: Fedex, UPS)
    shipping_cost = models.DecimalField(decimal_places=2, max_digits=12)  # cost of shipping the package
    weight = models.IntegerField(MinValueValidator(1))  # weight of the package

    class Meta:
        app_label = 'api_app'
        db_table = 'shipped_packages'

    def __unicode__(self):
        return self.content
